// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.




(MAINLOOP)
    @colorVal
    M=0     
    @rowCount
    M=0
    @rowOffset
    M=0
    @wordCount
    M=0

    @KBD
    D=M         // D = scan code
    @SETZERO
    D;JEQ       
    @colorVal
    M=-1
    @COLOR
    0;JMP

(SETZERO)
    @colorVal
    M=0
    @COLOR
    0;JMP

(COLOR)
    @rowCount
    D=M
    @256
    D=A-D
    @MAINLOOP
    D;JEQ

    @wordCount
    M=0
    @PAINTROW
    0;JMP

(UPDATEVARS)
    @32
    D=A
    @rowOffset
    M=M+D

    @rowCount
    M=M+1
    @COLOR
    0;JMP

(PAINTROW)
    @wordCount
    D=M
    @32
    D=A-D
    @UPDATEVARS
    D;JEQ

    @wordCount
    D=M
    @rowOffset
    D=D+M
    @SCREEN
    D=A+D
    @R0
    M=D
    @colorVal
    D=M
    @R0
    A=M
    M=D

    @wordCount
    M=M+1
    @PAINTROW
    0;JMP

(END)
    @MAINLOOP
    0;JMP