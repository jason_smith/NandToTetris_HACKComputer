    @rowCount
    M=0
    @rowOffset
    M=0
    @wordCount
    M=0

(COLOR)
    @rowCount
    D=M
    @256
    D=A-D
    @END
    D;JEQ

    @wordCount
    M=0
    @PAINTROW
    0;JMP

(UPDATEVARS)
    @32
    D=A
    @rowOffset
    M=M+D

    @rowCount
    M=M+1
    @COLOR
    0;JMP

(PAINTROW)
    @wordCount
    D=M
    @32
    D=A-D
    @UPDATEVARS
    D;JEQ

    @wordCount
    D=M
    @rowOffset
    D=D+M
    @SCREEN
    A=A+D
    M=-1

    @wordCount
    M=M+1
    @PAINTROW
    0;JMP

(END)
    @END
    0;JMP