// length = 10
// for (int i=0; i<length; i++)
//     array[i] = 12;

    // array address = 25
    @25
    D=A
    @array
    M=D

    // length = 10
    @10
    D=A
    @length
    M=D

    // i = 0
    @i
    M=0

(LOOP)
    @i
    D=M     // D = i
    @length
    D=M-D   // D = length - i
    @END
    D;JEQ   // if ((length - i) == 0) goto END

    @array
    D=M     // D = arrayAdress
    @i
    D=D+M   // D = arrayAdress +i
    @R0
    M=D     // RAM[0] = arrayAdress + i
    @12
    D=A     // D = 12
    @R0
    A=M     // A = arrayAdress + i
    M=D

    @i
    M=M+1   // i++

    @LOOP
    0;JMP

(END)
    @END
    0;JMP   // infinite loop